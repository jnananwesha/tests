# 3TIER Architecture

Can be shown as 

        Client
            |
            |
            |
        Frontend(Public Layer may be haproxy, Nginx LB, in AWS it can be Elastic Load balancer)  
            |
            |
            |
        Web Service (Application Layer, Example : web Service running on an EC2 instance)
            |
            |
            |
        DataBase (backend layer, in AWS, may be RDS DBs)


## Implementation 

Environment : 

On a Mac, 

        docker version
        Client: Docker Engine - Community
        Cloud integration: 1.0.9
        Version:           20.10.5

        docker-compose -v
        docker-compose version 1.28.5, build c4eb3a1f

Here a docker-compose file has been provided, which creates 3 docker containers

1. haproxy(Public layer)
2. Wordpress(Application Layer)
3. MYSQL (Database layer)

(there is a .env file which configures environment variables, it is needed for the whole setup to run)


# How to Run 

Download all the files in the repo, they all should be in the same folder. 

From the folder where you have these files, run, 

        docker-compose up

Check the running docker instances,

            docker ps
            CONTAINER ID   IMAGE              COMMAND                  CREATED          STATUS          PORTS                 NAMES
            18532d569f65   3tier_haproxy      "docker-entrypoint.s…"   20 minutes ago   Up 20 minutes   0.0.0.0:80->80/tcp    3tier_haproxy_1
            0787a7cc8f98   wordpress:latest   "docker-entrypoint.s…"   20 minutes ago   Up 20 minutes   80/tcp                3tier_wordpress_1
            dd0181a5de12   mysql:5.7          "docker-entrypoint.s…"   20 minutes ago   Up 20 minutes   3306/tcp, 33060/tcp   3tier_db_1

you should now get the access to frontend(haproxy)  by accessing ```http://localhost```
, then you should be able to access the wordpress and configure it.

Once you finish setting up the blog, it should remain there even if you delete the containers
as the data is on docker volumes, which are independent of the container. 

Each Layer is an independent component here and in a production environment, they can be independently
scaled up based on the requirement. 


docker-compose creates the below volumes and network 

        docker volume ls
        DRIVER    VOLUME NAME
        local     3tier_db_data  # DB volume
        local     3tier_wordpress_data # Wordpress Volume

        docker network ls
        NETWORK ID     NAME                   DRIVER    SCOPE
        e00a85ce623d   3tier_public_net       bridge    local

# Clean up the Environment

Delete the instances, from the project root(where all the docker related files are), run

        docker-compose stop 
        docker-compose rm 

Delete the volumes

        docker volume rm 3tier_db_data 3tier_wordpress_data

Delete the Network

        docker network rm 3tier_public_net 