#!/usr/bin/ruby 

require 'json'
require 'aws-sdk-ec2'
require 'optparse'

options = {}
parser = OptionParser.new do |opts|
            opts.banner = "Usage: ec2-client.rb [options]"
            opts.on('-p', '--property PROPERTY') { |o| options[:property] = o }
            opts.on('-h', '--help', 'Displays Help') do
                puts "
                If you pass a --property, only that property value will be returned, without one, all 
                will be diplayed.
                Properties  can be  ami_launch_index, image_id, instance_id, instance_type, key_name, launch_time, monitoring, placement, private_dns_name, private_ip_address, product_codes, public_dns_name, public_ip_address, state, state_transition_reason, subnet_id, vpc_id, architecture, block_device_mappings, client_token, :ebs_optimized, ena_support, hypervisor, network_interfaces, root_device_name, root_device_type, security_groups, source_dest_check, tags, virtualization_type, cpu_options, capacity_reservation_specification, hibernation_options, metadata_options, enclave_options"
                exit
            end 
end

parser.parse!

#Get the Instance ID 
ec2_instances=Aws::EC2::Resource.new(
    access_key_id: ENV['AWS_ACCESS_KEY_ID'],
    secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
    region: 'us-east-1'
)

@instance_id = ec2_instances.instances.to_a[0].instance_id


# EC2 client to interact 
ec2 = Aws::EC2::Client.new(
    access_key_id: ENV['AWS_ACCESS_KEY_ID'],
    secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
    region: 'us-east-1'
)

# Response for EC2 instance details 
response = ec2.describe_instances(
    instance_ids: [@instance_id]
)

node_details=response[:reservations][0].instances[0].to_h

if options[:property] == nil
    puts JSON.pretty_generate(node_details)

else
    opt=options[:property].intern
    begin
        puts node_details[opt]
    rescue StandardError => e
        puts "#{options[:property]} may not be one of properties that can be quried
        Please check help to get the right property"   
    end
end
