#  Using the Script 

Script uses AWS ruby SDK. It can surely be written with other Languages or 
just using aws cli in bash. When called without any option all the properties are 
returned in json format.

This script is tested against a one AWS instance. if there are many then,
we need call the instance_id array and it can be easily extended.

Script ec2-client.rb is tested with Ruby version

        ruby -v
        ruby 2.5.1p57 (2018-03-29 revision 63029) [x86_64-linux-gnu]

        gem list --local  | grep aws-sdk-ec2
        aws-sdk-ec2 (1.252.0)
        aws-sdk-ec2instanceconnect (1.15.0)

## How to Run 

```./ec2-client.rb``` => shows all the metadata in json 

Example: 
```bash
./ec2-client.rb
```bash
```json
{
  "ami_launch_index": 0,
  "image_id": "ami-0c2b8ca1dad447f8a",
  "instance_id": "i-03544395418223de1",
  "instance_type": "t2.micro",
  "key_name": "testpb",
  "launch_time": "2021-07-28 16:04:13 UTC",
  "monitoring": {
    "state": "disabled"
  },
  "placement": {
    "availability_zone": "us-east-1a",
    "group_name": "",
    "tenancy": "default"
  },
  "private_dns_name": "ip-172-31-9-252.ec2.internal",
  "private_ip_address": "172.31.9.252",
  "product_codes": [

  ],
  "public_dns_name": "ec2-3-237-204-146.compute-1.amazonaws.com",
  "public_ip_address": "3.237.204.146",
  "state": {
    "code": 16,
    "name": "running"
  },
  "state_transition_reason": "",
  "subnet_id": "subnet-70874116",
  "vpc_id": "vpc-19aebd63",
  "architecture": "x86_64",
  "block_device_mappings": [
    {
      "device_name": "/dev/xvda",
      "ebs": {
        "attach_time": "2021-07-28 16:04:14 UTC",
        "delete_on_termination": true,
        "status": "attached",
        "volume_id": "vol-0f1f3f78d4828fb92"
      }
    }
  ],
  "client_token": "",
  "ebs_optimized": false,
  "ena_support": true,
  "hypervisor": "xen",
  "network_interfaces": [
    {
      "association": {
        "ip_owner_id": "amazon",
        "public_dns_name": "ec2-3-237-204-146.compute-1.amazonaws.com",
        "public_ip": "3.237.204.146"
      },
      "attachment": {
        "attach_time": "2021-07-28 16:04:12 UTC",
        "attachment_id": "eni-attach-03c07c82a81675f23",
        "delete_on_termination": true,
        "device_index": 0,
        "status": "attached",
        "network_card_index": 0
      },
      "description": "",
      "groups": [
        {
          "group_name": "launch-wizard-2",
          "group_id": "sg-013a921adc45554e1"
        }
      ],
      "ipv_6_addresses": [

      ],
      "mac_address": "02:79:11:b0:4d:a3",
      "network_interface_id": "eni-01e910edefa208be8",
      "owner_id": "022741092737",
      "private_dns_name": "ip-172-31-9-252.ec2.internal",
      "private_ip_address": "172.31.9.252",
      "private_ip_addresses": [
        {
          "association": {
            "ip_owner_id": "amazon",
            "public_dns_name": "ec2-3-237-204-146.compute-1.amazonaws.com",
            "public_ip": "3.237.204.146"
          },
          "primary": true,
          "private_dns_name": "ip-172-31-9-252.ec2.internal",
          "private_ip_address": "172.31.9.252"
        }
      ],
      "source_dest_check": true,
      "status": "in-use",
      "subnet_id": "subnet-70874116",
      "vpc_id": "vpc-19aebd63",
      "interface_type": "interface"
    }
  ],
  "root_device_name": "/dev/xvda",
  "root_device_type": "ebs",
  "security_groups": [
    {
      "group_name": "launch-wizard-2",
      "group_id": "sg-013a921adc45554e1"
    }
  ],
  "source_dest_check": true,
  "tags": [
    {
      "key": "Name",
      "value": "Webserver"
    }
  ],
  "virtualization_type": "hvm",
  "cpu_options": {
    "core_count": 1,
    "threads_per_core": 1
  },
  "capacity_reservation_specification": {
    "capacity_reservation_preference": "open"
  },
  "hibernation_options": {
    "configured": false
  },
  "metadata_options": {
    "state": "applied",
    "http_tokens": "optional",
    "http_put_response_hop_limit": 1,
    "http_endpoint": "enabled"
  },
  "enclave_options": {
    "enabled": false
  }
}
```
## Showing only the selected property


```bash
/ec2-client.rb -p private_dns_name
ip-172-31-9-252.ec2.internal
```
```bash 
./ec2-client.rb -p block_device_mappings
{:device_name=>"/dev/xvda", :ebs=>{:attach_time=>2021-07-28 16:04:14 UTC, :delete_on_termination=>true, :status=>"attached", :volume_id=>"vol-0f1f3f78d4828fb92"}}
```

```bash
./ec2-client.rb -p public_ip_address
3.237.204.146
```

### Displaying help 
```bash
./ec2-client.rb -h

                If you pass a --property, only that property value will be returned, without one all
                will be diplayed.
                Properties  can be  ami_launch_index, image_id, instance_id, instance_type, key_name, launch_time, monitoring, placement, private_dns_name, private_ip_address, product_codes, public_dns_name, public_ip_address, state, state_transition_reason, subnet_id, vpc_id, architecture, block_device_mappings, client_token, :ebs_optimized, ena_support, hypervisor, network_interfaces, root_device_name, root_device_type, security_groups, source_dest_check, tags, virtualization_type, cpu_options, capacity_reservation_specification, hibernation_options, metadata_options, enclave_options
```
