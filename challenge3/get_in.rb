#!/usr/bin/ruby 

def get_in(options = {}, keys)
    puts "Nested value passed: #{options}"
    puts "keys requested => " + keys  
    test=[]
    keys.split("/").each do | i | 
        test.append(i.intern)
    end 
   puts "Value is: " + options[test[0]][test[1]][test[2]]
end

object = {"a":{"b":{"c":"d"}}}
get_in(object, 'a/b/c')

object = {"x":{"y":{"z":"a"}}}
get_in(object, 'x/y/z')