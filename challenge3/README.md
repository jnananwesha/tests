# Running the script

```bash
./get_in.rb
Nested value passed: {:a=>{:b=>{:c=>"d"}}}
keys requested => a/b/c
Value is: d
Nested value passed: {:x=>{:y=>{:z=>"a"}}}
keys requested => x/y/z
Value is: a
```

# Running in pry 

```bash
[1] pry(main)> def get_in(options = {}, keys)
[1] pry(main)*   puts "Nested value passed: #{options}"
[1] pry(main)*   puts "keys requested => " + keys
[1] pry(main)*   test=[]
[1] pry(main)*   keys.split("/").each do | i |
[1] pry(main)*     test.append(i.intern)
[1] pry(main)*   end
[1] pry(main)*   puts "Value is: " + options[test[0]][test[1]][test[2]]

[1] pry(main)* end
=> :get_in
[2] pry(main)> object = {"a":{"b":{"c":"d"}}}
=> {:a=>{:b=>{:c=>"d"}}}
[3] pry(main)> get_in(object, 'a/b/c')
Nested value passed: {:a=>{:b=>{:c=>"d"}}}
keys requested => a/b/c
Value is: d
=> nil
[4] pry(main)> object = {"x":{"y":{"z":"a"}}}
=> {:x=>{:y=>{:z=>"a"}}}
[5] pry(main)> get_in(object, 'x/y/z')
Nested value passed: {:x=>{:y=>{:z=>"a"}}}
keys requested => x/y/z
Value is: a
=> nil
```

### Second Script

```get_in_2.rb``` does the same.

        [21] pry(main)> def get_in(options = {}, keys)
        [21] pry(main)*   puts "Nested value passed: #{options}"
        [21] pry(main)*   puts "keys requested => " + keys
        [21] pry(main)*   temp_obj = options
        [21] pry(main)*   keys_split=keys.split("/")
        [21] pry(main)*   keys_split.each do | key |
        [21] pry(main)*     temp_obj = temp_obj[key.intern]
        [21] pry(main)*   end
        [21] pry(main)*   return temp_obj
        [21] pry(main)* end
        => :get_in
        [22] pry(main)> get_in(object, 'a/b/c')
        Nested value passed: {:a=>{:b=>{:c=>"d"}}}
        keys requested => a/b/c
        => "d"
        [23] pry(main)> object = {"x":{"y":{"z":"a"}}}
        => {:x=>{:y=>{:z=>"a"}}}
        [24] pry(main)> get_in(object, 'x/y/z')
        Nested value passed: {:x=>{:y=>{:z=>"a"}}}
        keys requested => x/y/z
        => "a"
