#!/usr/bin/ruby
def get_in(options = {}, keys)
	puts "Nested value passed: #{options}"
    puts "keys requested => " + keys
    temp_obj = options
    keys_split=keys.split("/")
    keys_split.each do | key |
        temp_obj = temp_obj[key.intern]
    end 
    puts temp_obj
end 

object = {"a":{"b":{"c":"d"}}}
get_in(object, 'a/b/c')

object = {"x":{"y":{"z":"a"}}}
get_in(object, 'x/y/z')